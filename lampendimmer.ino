int
  pin1 = 2,
  pin2 = 3,
  pin3 = 4;
int
  pin_dimm[] = {5, 6};

int power[] = {4, 4};
// A capacicator loads faster, when empty, so I use exponential growth in values.
int values[] = {0, 2, 4, 8, 16, 32, 64, 128, 255};
int target = 0;
int max_target = sizeof(power)/sizeof(power[0]);
int values_count = sizeof(values)/sizeof(values[0]) - 1;

unsigned long time1_up, time2_up, time3_up;
unsigned long time1_down, time2_down, time3_down;

bool is_down1, is_down2, is_down3;
bool is_down1_p, is_down2_p, is_down3_p;


enum
{
  WAITING_CCW,
  WAITING_CW,
  WAITING
} status = WAITING;


void setup() {
  time1_up = micros();
  time2_up = micros();
  time3_up = micros();
  time1_down = micros();
  time2_down = micros();
  time3_down = micros();
  pinMode(pin1, INPUT_PULLUP);
  pinMode(pin2, INPUT_PULLUP);
  pinMode(pin3, INPUT_PULLUP);
  pinMode(pin_dimm[0], OUTPUT);
  pinMode(pin_dimm[1], OUTPUT);
//  Serial.begin(9600);
//  Serial.println("test");

  for(int i = 0; i < max_target; i++)
  {
    analogWrite(pin_dimm[i], values[power[i]]);
  }
}

void loop() {
  
  //char buffer[128];
  bool send_intensities = false;

  if(digitalRead(pin1) != HIGH)
    time1_up = micros();
  else
    time1_down = micros();
  if(digitalRead(pin2) != HIGH)
    time2_up = micros();
  else
    time2_down = micros();
  if(digitalRead(pin3) != HIGH)
    time3_up = micros();
  else
    time3_down = micros();
  
  is_down1_p = is_down1;
  is_down2_p = is_down2;
  is_down3_p = is_down3;
  is_down1 = (long) time1_up - (long) time1_down > 125;
  is_down2 = (long) time2_up - (long) time2_down > 125;
  is_down3 = (long) time3_up - (long) time3_down > 125;

  if(time2_down == 0 || time3_down == 0
    || time2_up == 0 || time3_up == 0)
  {
    return;
  }
  if((is_down3_p != is_down3) && is_down3)
  {
    switch(status)
    {
      case WAITING:
        if(!is_down2)
          status = WAITING_CCW;
      break;
      case WAITING_CW:
        //Serial.println("l");
        if(is_down2)
        {
          power[target] -= 1;
          if(power[target] < 0)
            power[target] = 0;
          send_intensities = true;
          status = WAITING;
          time2_up = time3_up = 0;
          time2_down = time3_down = 0;
        }
      break;
      case WAITING_CCW:
        status = WAITING;
      break;
      default:
      //  Serial.println("!");
      break;
    }
  }
  if((is_down2_p != is_down2) && is_down2)
  {
    switch(status)
    {
      case WAITING:
      if(!is_down3)
        status = WAITING_CW;
      break;
      case WAITING_CW:
        status = WAITING;
      break;
      case WAITING_CCW:
        //Serial.println("r");
        if(is_down3)
        {
          power[target] += 1;
          if(power[target] > values_count)
            power[target] = values_count;
          send_intensities = true;
          status = WAITING;
          time2_up = time3_up = 0;
          time2_down = time3_down = 0;
        }
      break;
      default:
        //Serial.println("!");
      break;
    }
  }
  if(is_down1 != is_down1_p && time1_down != 0 && time1_up != 0)
  {
    if(is_down1)
    {
      //Serial.println("p");
      target++;
      if(target == max_target)
      {
        target = 0;
      }
    }
    //else
      //Serial.println("u");
  }
  
  if(send_intensities)
    for(int i = 0; i < max_target; i++)
    {
      analogWrite(pin_dimm[i], values[power[i]]);
    }
}
