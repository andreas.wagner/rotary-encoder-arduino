int pin1 = 2, pin2 = 3, pin3 = 4;

unsigned long time1_up, time2_up, time3_up;
unsigned long time1_down, time2_down, time3_down;

bool is_down1, is_down2, is_down3;
bool is_down1_p, is_down2_p, is_down3_p;


enum
{
  WAITING_CCW,
  WAITING_CW,
  WAITING
} status = WAITING;


void setup() {
  time1_up = micros();
  time2_up = micros();
  time3_up = micros();
  time1_down = micros();
  time2_down = micros();
  time3_down = micros();
  pinMode(pin1, INPUT_PULLUP);
  pinMode(pin2, INPUT_PULLUP);
  pinMode(pin3, INPUT_PULLUP);
  Serial.begin(9600);
  Serial.println("test");
}

void loop() {
  
  //char buffer[128];

  if(digitalRead(pin1) != HIGH)
    time1_up = micros();
  else
    time1_down = micros();
  if(digitalRead(pin2) != HIGH)
    time2_up = micros();
  else
    time2_down = micros();
  if(digitalRead(pin3) != HIGH)
    time3_up = micros();
  else
    time3_down = micros();
  
  is_down1_p = is_down1;
  is_down2_p = is_down2;
  is_down3_p = is_down3;
  is_down1 = (((long) time1_up - (long) time1_down) > 125); // 125 Mikrosekunden Entprellzeit
  is_down2 = (((long) time2_up - (long) time2_down) > 125);
  is_down3 = (((long) time3_up - (long) time3_down) > 125);

  if((is_down3_p != is_down3) && is_down3)
  {
    switch(status)
    {
      case WAITING:
        if(!is_down2)
          status = WAITING_CCW;
      break;
      case WAITING_CW:
        if(is_down2)
        {
          Serial.println("l");
          status = WAITING;
        }
      break;
      case WAITING_CCW:
        status = WAITING;
      break;
      default:
        Serial.println("!");
      break;
    }
  }
  if((is_down2_p != is_down2) && is_down2)
  {
    switch(status)
    {
      case WAITING:
        if(!is_down3)
          status = WAITING_CW;
      break;
      case WAITING_CW:
        status = WAITING;
      break;
      case WAITING_CCW:
        if(is_down3)
        {
          Serial.println("r");
          status = WAITING;
        }
      break;
      default:
        Serial.println("!");
      break;
    }
  }
  if(is_down1 != is_down1_p)
  {
    if(is_down1)
      Serial.println("p");
    else
      Serial.println("u");
  }

}
